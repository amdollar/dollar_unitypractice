﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (input.GetKey(KeyCode.UpArrow)){
            transform.position += Vector3.up * Time.deltaTime * moveSpeed;
        }
        if (input.GetKey(KeyCode.DownArrow))
        {
            transform.position += Vector3.down * Time.deltaTime * moveSpeed;
        }
    }
}
