﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {

    Rigidbody rb;
    public float speed;
    public Text countText;
    public Text winText;
    int count = 0;
    bool dead = false;
    Vector3 wind;
    //public GameObject cube;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
        
	}
	
	// Update is called once per frame
	void Update () {
		if (dead ==false && transform.position.y <= -5)
        {
            dead = true;
            GetComponent<MeshRenderer>().enabled = false;
            Invoke("RestartLevel", 1f);
        }
	}
    private void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0f, moveVertical);
  
        rb.AddForce(movement * Time.deltaTime * speed);
        rb.AddForce(wind * Time.deltaTime);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pickup"))
        {
            count += 1;
            SetCountText();
            Destroy(other.gameObject);
            //if(count == 4)
            //{
             //   cube.enabled = false;
            //}
        }
        // Wind physics (not working yet)
        if (other.gameObject.CompareTag("Wind"))
        {
            
            wind = new Vector3(0f, 1000f, 0f);   
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Wind"))
        {

            wind = new Vector3(0f, 0f, 0f);
        }
    }
            void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();

        if (count >= 12){
            winText.text = "You Win!";
        }
    }
}
